package pl.edu.pwsztar.domain.mapper;

import pl.edu.pwsztar.domain.dto.CreateMovieDto;
import pl.edu.pwsztar.domain.entity.Movie;

public class MovieAddMapper {

    public Movie convertToDto(CreateMovieDto createMovieDto) {
        Movie newMovie =  new Movie();
        newMovie.setTitle(createMovieDto.getTitle());
        newMovie.setYear(createMovieDto.getYear());
        newMovie.setImage(createMovieDto.getImage());

        return newMovie;
    }
}
